<?php
	$title = 'Student';
	require_once('header.php');

	if (isset($_POST['submit'])) {
		if (!post_check('student_id') or !post_check('student_name') or !post_check('student_session') or
				!post_check('student_email') or !post_check('student_mobile') or !post_check('student_parent_mobile') or
				!post_check('student_bod') or !post_check('student_gender') or !post_check('student_address')) {
			$type = 'danger';
			$message = "Please provide all the information";
		} else {
			$student_id = $_POST['student_id'];
			$student_name = $_POST['student_name'];
			$student_session = $_POST['student_session'];
			$student_email = $_POST['student_email'];
			$student_mobile = $_POST['student_mobile'];
			$student_parent_mobile = $_POST['student_parent_mobile'];
			$student_bod = $_POST['student_bod'];
			$student_gender = ucfirst(strtolower($_POST['student_gender']));
			$student_address = $_POST['student_address'];
			$department_id = $_SESSION['deptid'];

			try {
				// Check student existence
				$query = "SELECT * FROM students_info WHERE students_roll=?";
				$stmt = $connection->prepare($query);
				$stmt->bind_param("s", $student_id);
				$stmt->execute();
				$num_rows = $stmt->get_result()->num_rows;
				$stmt->close();
				if ($num_rows > 0) {
					$type = 'danger';
					$message = 'Student already registed.';
				} else {
					$query = "INSERT INTO `students_info`(
															`students_name`, `students_roll`, `students_session`, `students_email`,
															`students_contact`, `students_parent_contact`, `students_date_of_birth`, `students_gender`,
															`students_address`, `department_info_iddepartment_info`) 
							VALUES (?,?,?,?,?,?,?,?,?,?)";

					$stmt = $connection->prepare($query);
					$stmt->bind_param("sssssssssi",
						$student_name,
						$student_id,
						$student_session,
						$student_email,
						$student_mobile,
						$student_parent_mobile,
						$student_bod,
						$student_gender,
						$student_address,
						$department_id
					);
					$stmt->execute();
					$stmt->close();

					$query = "INSERT INTO activity_history (activity_description, department_info_iddepartment_info) VALUES (?, ?)";
					$stmt = $connection->prepare($query);
					$desc = "Added new student";
					$stmt->bind_param("si", $desc, $_SESSION['deptid']);
					$stmt->execute();
					$stmt->close();

					$type = 'success';
					$message = "Registration successful";
				}
			} catch (Exception $ex) {
				$type = 'danger';
				$message = "Registration failed";
			}
		}
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top" xmlns="http://www.w3.org/1999/html">
<!-- Navigation bar -->
<?php require_once('navbar.php'); ?>

<!-- Body -->
<div class="container">
	<?php
		if (isset($type)) {
			?>
			<div class="row no-gutters">
				<div class="col-lg-5 col-md-12 ml-auto">
					<div class="alert alert-<?php echo $type; ?> alert-dismissible fade show"
							 role="alert">
						<strong><?php echo $message; ?></strong>
					</div>
				</div>
			</div>
			<script>
				$(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function () {
					$(".alert-dismissible").alert('close');
				});
			</script>
			<?php
		}
	?>
	<div class="row justify-content-center mb-3">
		<div class="mt-4 pt-4 pl-2 pr-3 text-justify">
			<div class="text-center">
				<h4>Student Registration Form</h4>
			</div>
			<form class="shadow-lg mt-3 p-5 mb-3" action="" method="POST">
				<div class="form-group">
					<input type="text" name="student_id" id="student_id" class="form-control" placeholder="Enter student id"
								 required autofocus>
				</div>
				<div class="form-group">
					<input type="text" name="student_name" id="student_name" class="form-control" placeholder="Enter student name"
								 required>
				</div>
				<div class="form-group">
					<label for="session">Select Session</label>
					<select class="form-control custom-select" name="student_session" id="student_session" required>
						<option value="" selected disabled hidden>None</option>
						<?php
							$query = "SELECT session_name FROM session_info WHERE department_info_iddepartment_info=? ORDER BY session_name";
							try {
								$stmt = $connection->prepare($query);
								$stmt->bind_param("i", $_SESSION['deptid']);
								$stmt->execute();
								$result = $stmt->get_result();
								while ($row = $result->fetch_row()) {
									echo "<option value='$row[0]'>$row[0]</option>";	
								}
								$stmt->close();
							} catch (Exception $ex) {}
						?>
					</select>
				</div>
				<div class="form-group">
					<input type="email" name="student_email" id="student_email" class="form-control"
								 placeholder="Enter student email" required>
				</div>
				<div class="form-group">
					<input type="tel" pattern="0[0-9]{10}" name="student_mobile" id="student_mobile" class="form-control"
								 placeholder="Enter student mobile number" required>
				</div>
				<div class="form-group">
					<input type="tel" pattern="0[0-9]{10}" name="student_parent_mobile" id="student_parent_mobile"
								 class="form-control" placeholder="Enter parent mobile number" required>
				</div>

				<div class="form-group">
					<label for="student_bod">Date of birth: </label>
					<input type="date" name="student_bod" id="student_bod" class="form-control custom-select" required>
				</div>

				<!-- Gender -->
				<div class="form-group">
					<label>Select Gender: </label>
					<div class="row">
						<div class="custom-control custom-radio">
							<input type="radio" id="student_gender_male" name="student_gender" value="Male" required checked>
							<label for="student_gender_male">Male</label>
						</div>
						<div class="custom-control custom-radio">
							<input type="radio" id="student_gender_female" name="student_gender" value="Female" required>
							<label for="student_gender_female">Female</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<textarea name="student_address" id="student_address" class="form-control" rows="4"
										placeholder="Write your address"></textarea>
				</div>

				<div class="form-group">
					<input type="submit" name="submit" class="btn btn-primary btn-block" value="Submit">
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Footer -->
<?php require_once('footer.php'); ?>
</body>

</html>
