$(document).ready(function() {
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $('#changeToggleIcon').toggleClass("navbar-toggler-off-icon");
    $('#changeToggleIcon').toggleClass("navbar-toggler-icon");
  });
});
