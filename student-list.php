<?php
	$title = 'Student';
	require_once('header.php');
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
<!-- Navigation bar -->
<?php require_once('navbar.php'); ?>

<!-- Body -->
<div class="container">
	<div class="row">
		<div class="col mt-2 pt-2 pr-3 text-justify">
			<h1>Student List</h1>
			<div class="container mt-3">
					<form action="#student_table" method="post">
						<div class="row">
								<div class="form-group ml-2 mr-2">
									<label for="session">Select Session</label>
									<select class="form-control custom-select" name="session" id="session" required>
										<option value="" selected disabled hidden>None</option>
										<?php
											$query = "SELECT session_name FROM session_info WHERE department_info_iddepartment_info=? ORDER BY session_name";
											try {
												$stmt = $connection->prepare($query);
												$stmt->bind_param("i", $_SESSION['deptid']);
												$stmt->execute();
												$result = $stmt->get_result();
												while ($row = $result->fetch_row()) {
													echo "<option value='$row[0]'>$row[0]</option>";
												}
												$stmt->close();
											} catch (Exception $ex) {}
										?>
									</select>
								</div>
							<div class="form-group ml-2 mr-2 mt-4">
								<button type="submit" name="submitSessionTerm" id="submitSessionTerm"
												class="btn btn-primary">Submit Query
								</button>
							</div>
						</div>
					</form>
			</div>

			<div id="student_table" class="container mt-3">
				<?php
					if (post_check('session')
						//  and post_check('term')
					) {
						echo "<div class='row'>";
						$safe_sess = mysqli_real_escape_string($connection, $_POST['session']);
						// $safe_term = mysqli_real_escape_string($connection, $_POST['term']);

						unset($_POST['session']);
						unset($_POST['term']);

						$query = "SELECT students_roll, students_name, students_contact, students_email FROM students_info WHERE students_session='" . $safe_sess . "' and department_info_iddepartment_info='" . $_SESSION['deptid'] . "' ORDER BY CAST(SUBSTRING(students_roll, 4, 2) AS INT) DESC, CAST(SUBSTRING(students_roll, 6, 5) AS INT)";
						$data = mysqli_query($connection, $query);
						$count = mysqli_num_rows($data);
					if ($count > 0) {
						?>
						<form id="postFileView" action="view.php" method="post">
							<input type="hidden" name="selectedFileID" id="selectedFileID">
							<table class='table table-bordered table-condensed table-hover'>
								<thead class='thead-light'>
								<tr>
									<th>SL</th>
									<th>Student ID</th>
									<th>Student Name</th>
									<th>Student Contact Number</th>
									<th>Student Email</th>
								</tr>
								</thead>
								<tbody class="text-center">
								<?php
									}
									$sl = 1;
									while ($row = $data->fetch_row()) {
										echo "<tr class='table-row' onclick='postFileInfo(this);'>
                    <td>$sl</td>
                    <td>$row[0]</td>
                    <td>" . ucwords(strtolower($row[1])) . "</td>
                    <td>$row[2]</td>
                    <td>$row[3]</td>";
										$sl++;
									}

									if ($count > 0) {
								?>
								</tbody>
							</table>
						</form>

						<script>
							function postFileInfo(clickedRow) {
								var filetd = $(clickedRow).find('td').eq(1).html();
								if (!(filetd.trim() === null)) {
									$('#selectedFileID').val(filetd);
									$('#postFileView').submit();
								}
							}
						</script>
						<?php
					} else {
						echo "<p class='text-danger'>No students</p>";
					}
						echo "</div>";
					}
				?>
			</div>
		</div>
	</div>
</div>
</div>
</div>

<!-- Footer -->
<?php require_once('footer.php'); ?>
</body>

</html>