<?php
// Setting variables for page
$title='Results';

require_once('header.php');
 ?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
  <!-- Navigation bar -->
  <?php require_once('navbar.php'); ?>

  <!-- Body -->
  <div class="container">
    <div class="row">
      <div class="mt-4 pt-4 pl-2 pr-3 text-justify">
        <h1 class="mt-4">Results</h1>
        <p>See student results</p>
        <div class="container mt-5">
          <div class="row">
            <form class="row" action="#result_table" method="post">
              <div class="form-group col">
                <label for="session">Select Session</label>
                <select class="custom-select" name="session" id="session" required>
                  <option value="" selected disabled>Session</option>
                  <option value="2017-2018">2017-2018</option>
                  <option value="2018-2019">2018-2019</option>
                  <option value="2019-2020">2019-2020</option>
                </select>
              </div>
              <div class="form-group col">
                <label for="term">Select Term</label>
                <select class="custom-select" name="term" id="term" required>
                  <option value="" selected disabled>Term</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                </select>
              </div>

              <div class="form-group col mt-2">
                <button type="button" name="submitSessionTerm" id="submitSessionTerm" class="btn btn-primary mt-4">Submit Query</button>
              </div>
            </form>
          </div>
        </div>

        <div id="result_table" class="container mt-5">
          <p id="status"></p>
          <div class="row">
            <table class="table table-stripped table-bordered">
              <!-- Populate thead with course list and tbody with students
              name, course results -->
              <thead class="thead-light">
                <tr>
                  <th>Name</th>
                  <th>CSE3101</th>
                  <th>CSE3103</th>
                  <th>CSE3104</th>
                  <th>CSE3105</th>
                  <th>CSE3106</th>
                  <th>BUS3107</th>
                  <th>BUS3108</th>
                  <th>SE3109</th>
                  <th>SE3110</th>
                  <th>SE3111</th>
                  <th>SE3112</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>STUDENT 1</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                </tr>
                <tr>
                  <td>STUDENT 2</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                </tr>
                <tr>
                  <td>STUDENT 3</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                </tr>
                <tr>
                  <td>STUDENT 4</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                </tr>
                <tr>
                  <td>STUDENT 5</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                </tr>
                <tr>
                  <td>STUDENT 6</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                  <td>-- TGPA</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <?php require_once('footer.php'); ?>
</body>

</html>
