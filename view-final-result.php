<?php
	$title = 'Final Result';

	require_once('header.php');
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row">
			<div class="col mt-2 pt-2 pr-3 text-justify">
				<h1>Results</h1>
				<p>See student results (TGPA)</p>
				<div class="container mt-5">
					<form action="#result_table" method="post">
						<div class="row">
							<div class="form-group ml-2 mr-2">
								<label for="session">Select Session</label>
								<select class="form-control custom-select" name="session" id="session" required>
									<option value="" selected disabled hidden>None</option>
									<?php
										$query = "SELECT session_name FROM session_info WHERE department_info_iddepartment_info=? ORDER BY session_name";
										try {
											$stmt = $connection->prepare($query);
											$stmt->bind_param("i", $_SESSION['deptid']);
											$stmt->execute();
											$result = $stmt->get_result();
											while ($row = $result->fetch_row()) {
												echo "<option value='$row[0]'>$row[0]</option>";	
											}
											$stmt->close();
										} catch (Exception $ex) {}
									?>
								</select>
							</div>
							<div class="form-group ml-2 mr-2">
								<label for="year">Select year</label>
								<select class="form-control custom-select" name="year" id="year" required>
									<option value="" selected disabled hidden>Year</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
								</select>
							</div>
							<div class="form-group ml-2 mr-2">
								<label for="term">Select Term</label>
								<select class="form-control custom-select" name="term" id="term" required>
									<option value="" selected disabled>None</option>
									<option value="1">1</option>
									<option value="2">2</option>
								</select>
							</div>
						</div>

						<div class="form-group mr-2">
							<button type="submit" name="submitSessionTerm" id="submitSessionTerm" class="btn btn-primary">Submit Query
							</button>
						</div>
					</form>
				</div>

				<div id="result_table" class="container mt-5">
					<?php
					if (isset($_POST['session']) and isset($_POST['term']) and isset($_POST['year'])) {
						?>
					<div class="row">
						<table class="table table-stripped table-bordered">
							<thead class="thead-light">
								<?php
									$query = "SELECT * FROM results_info WHERE results_session=? AND results_year=? AND results_term=? AND department_info_iddepartment_info=?";
									try {
										$stmt = $connection->prepare($query);
										$stmt->bind_param("sssi", $_POST['session'], $_POST['year'], $_POST['term'], $_SESSION['deptid']);
										$stmt->execute();
										$result = $stmt->get_result();
										$num_rows = $result->num_rows;
										$stmt->close();

										$found = FALSE;

										if ($num_rows > 0) {
											$found = TRUE;

											$row = $result->fetch_assoc();
											$filename = $row['results_table_name'];
											$filepath = 'uploads/results/' . $filename;
											$temp = explode('.', $filename);
											$ext = end($temp);

											if ($ext === 'csv') {
												if (($file = fopen($filepath, 'r')) !== FALSE) {
													set_time_limit(0);
													$sep = ',';

													$firstline = fgets($file, 4096);
													//Gets the number of fields, in CSV-files the names of the fields are mostly given in the first line
													$num = strlen($firstline) - strlen(str_replace($sep, "", $firstline));

													//save the different fields of the firstline in an array called fields
													$fields = array();
													$fields = explode($sep, $firstline, ($num + 1));

													$line = array();
													$i = 0;
													$dsatz = array();

													//CSV: one line is one record and the cells/fields are seperated by ";"
													//so $dsatz is an two dimensional array saving the records like this: $dsatz[number of record][number of cell]
													while ($line[$i] = fgets($file, 4096)) {
														$dsatz[$i] = array();
														$dsatz[$i] = explode($sep, $line[$i], ($num + 1));

														$i++;
													}

													echo "<tr>";
													for ($k = 0; $k != ($num + 1); $k++) {
														echo "<th>" . $fields[$k] . "</th>";
													}
													echo "</tr>";
													echo "</thead>";
													echo "<tbody>";
													foreach ($dsatz as $key => $number) {
														//new table row for every record
														echo "<tr>";
														foreach ($number as $k => $content) {
															//new table cell for every field of the record
															echo "<td>" . $content . "</td>";
														}
														echo "</tr>";
													}
													echo "</tbody>";

													fclose($file);
												} else {
													echo 'cannot open file';
													echo "</thead>";
												}
											} else {
												echo "</thead>";
											}
										}

										if (!$found) {
											echo "<tr><th class='text-center'>No result found.</th></tr></thead>";
										}
									} catch (Exception $ex) {
										echo "<tr>ERROR!</tr></thead>";
									}
								?>
						</table>
					</div>
					<?php
					}
				?>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php require_once('footer.php'); ?>
</body>

</html>