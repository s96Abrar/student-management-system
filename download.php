<?php

	if (isset($_REQUEST['link'])) {
		$file = $_REQUEST['link'];
		$filename = basename($file);
		$temp = explode('.', $filename);
		$ext = end($temp);

		if ($ext === 'csv') {
			if ($filename === 'template_routine.csv' or 
					$filename === 'template_result.csv' or 
					$filename === 'template_ctexam.csv') {
				if (file_exists($file)) {
					//Define header information
					header('Content-Description: File Transfer');
					// header('Content-Type: application/octet-stream');
					header('Content-Type: text/csv');
					header("Cache-Control: no-cache, must-revalidate");
					header("Expires: 0");
					header('Content-Disposition: attachment; filename="' . basename($file) . '"');
					header('Content-Length: ' . filesize($file));
					header('Pragma: public');

					//Clear system output buffer
					flush();

					//Read the size of the file
					readfile($file);

					//Terminate from the script
					header('Location: index.php');
				} else {
					echo "<script>alert('File does not exist.')</script>";
				}
			} else {
				echo "<script>alert('Invalid request.')</script>";
			}
		}
	}