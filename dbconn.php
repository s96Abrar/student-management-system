<?php
	require_once('config.php');

	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	try {
		$connection = new mysqli(Config::$dbhost, Config::$dbuser, Config::$dbpass, Config::$dbname);
		$connection->set_charset("utf8mb4");
	} catch (Exception $e) {
		error_log($e->getMessage());
		exit('Error connecting to database.'); //Should be a message a typical user could understand
	}
