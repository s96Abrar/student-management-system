<?php
// Setting variables for page
$title='CT Exam';

require_once('header.php');
 ?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
  <!-- Navigation bar -->
  <?php require_once('navbar.php'); ?>

  <!-- Body -->
  <div class="container">
    <div class="row">
      <div class="mt-4 pt-4 pl-2 pr-3 text-justify">
        <h1 class="mt-4">CT Exams</h1>
        <p>See CT exams results</p>
        <div class="container mt-5">
          <div class="row">
            <form class="row" action="#result_table" method="post">
              <div class="form-group col">
                <label for="session">Select Session</label>
                <select class="custom-select" name="session" id="session" required>
                  <option value="" selected disabled>Session</option>
                  <option value="2017-2018">2017-2018</option>
                  <option value="2018-2019">2018-2019</option>
                  <option value="2019-2020">2019-2020</option>
                </select>
              </div>
              <div class="form-group col">
                <label for="term">Select Term</label>
                <select class="custom-select" name="term" id="term" required>
                  <option value="" selected disabled>Term</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                </select>
              </div>
              <div class="form-group col">
                <label for="term">Select Couese Code</label>
                <select class="custom-select" name="term" id="course_code" required>
                  <option value="" selected disabled>Course Code</option>
                  <!-- Populate course code by slection change event of session and term -->
                  <option value="CSE3101">CSE3101</option>
                  <option value="CSE3103">CSE3103</option>
                  <option value="CSE3104">CSE3104</option>
                  <option value="CSE3105">CSE3105</option>
                  <option value="CSE3106">CSE3106</option>
                </select>
              </div>

              <div class="form-group col mt-2">
                <button type="button" name="submitSessionTermCourse" id="submitSessionTermCourse" class="btn btn-primary mt-4">Submit Query</button>
              </div>
            </form>
          </div>
        </div>

        <div id="result_table" class="container mt-5">
          <p id="status"></p>
          <div class="row">
            <table class="table table-stripped table-bordered">
              <thead class="thead-light">
                <tr>
                  <th>Name</th>
                  <th>CT1</th>
                  <th>CT2</th>
                  <th>CT3</th>
                  <th>Total</th>
                  <th>Best two Avg.</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>STUDENT 1</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                </tr>
                <tr>
                  <td>STUDENT 2</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                </tr>
                <tr>
                  <td>STUDENT 3</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                </tr>
                <tr>
                  <td>STUDENT 4</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                </tr>
                <tr>
                  <td>STUDENT 5</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                </tr>
                <tr>
                  <td>STUDENT 6</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                  <td>--</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <?php require_once('footer.php'); ?>
</body>

</html>
