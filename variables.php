<?php
// This file contains the page title and link names

$pagetitle = [
  'Dashboard',
  'Teacher',
  'Student',
  'Attendance',
  'CT Exam',
  'Results',
  'Class Routine'
]

$pagelink = [
  'index.php',
  'teacher.php',
  'student.php',
  'attendance.php',
  'ct-exam.php',
  'results.php',
  'class-routine.php'
]
 ?>
