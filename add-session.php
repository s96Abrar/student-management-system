<?php
	$title = 'Session';
	require_once('header.php');

	if (isset($_POST['submit'])) {
		if (post_check('session') and post_check('batch')) {
			$session = $_POST['session'];
			$batch = $_POST['batch'];

			$t = explode('-', $session);
			$t1 = $t[0];
			$t2 = $t[1];
			$t = $t2 - $t1;
			if ($t === 1) {
				if (preg_match("/^20[0-9]{2}-20[0-9]{2}$/", $session) === 1 and
						preg_match("/^[0-9]{2}$/", $batch) === 1) {
					try {
						$query = "SELECT * FROM session_info WHERE session_name=? AND session_batch=? AND department_info_iddepartment_info=?";
						$stmt = $connection->prepare($query);
						$stmt->bind_param("ssi", $session, $batch, $_SESSION['deptid']);
						$stmt->execute();
						$num_rows = $stmt->get_result()->num_rows;
						$stmt->close();
						if ($num_rows > 0) {
							$type = 'danger';
							$message = 'There is already a session exists';
						} else {
							$query = "INSERT INTO session_info VALUES (NULL, ?, ?, ?)";
							$stmt = $connection->prepare($query);
							$stmt->bind_param("ssi", $session, $batch, $_SESSION['deptid']);
							$insert = $stmt->execute();
							if ($insert) {
								$query = "INSERT INTO activity_history (activity_description, department_info_iddepartment_info) VALUES (?, ?)";
								$stmt = $connection->prepare($query);
								$desc = "Added new session " . $_POST['session'];
								$stmt->bind_param("si", $desc, $_SESSION['deptid']);
								$stmt->execute();
								$stmt->close();

								$type = 'success';
								$message = 'Successfully added new session';
							} else {
								$type = 'danger';
								$message = 'Cannot add new session';
							}
							$stmt->close();
						}
					} catch (Exception $ex) {
						$type = 'danger';
						$message = 'Query Error';
					}
				} else {
					$type = 'danger';
					$message = 'Please match the requested match for the fields';
				}
			} else {
				$type = 'danger';
				$message = 'Session difference is not correct';
			}
		} else {
			$type = 'danger';
			$message = 'Please provide all the information.';
		}
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<?php
		if (isset($type)) {
			?>
		<div class="row no-gutters">
			<div class="col-lg-5 col-md-12 ml-auto">
				<div class="alert alert-<?php echo $type; ?> alert-dismissible fade show" role="alert">
					<strong><?php echo $message; ?></strong>
				</div>
			</div>
		</div>
		<script>
		$(".alert-dismissible").fadeTo(5000, 500).slideUp(500, function() {
			$(".alert-dismissible").alert('close');
		});
		</script>
		<?php
		}
	?>
		<div class="row justify-content-center mb-3">
			<div class="mt-2 pt-2 pr-3 text-justify">
				<div class="mt-4 text-center">
					<h4>Add new session</h4>
				</div>
				<form class="shadow-lg mt-3 p-5 mb-3 rounded-lg" action="" method="POST">
					<div class="form-group">
						<label for="session">Enter session</label>
						<input type="text" name="session" id="session" class="form-control" placeholder="e.g. 2017-2018" required
							pattern="20[0-9]{2}-20[0-9]{2}" autofocus>
					</div>
					<div class="form-group">
						<label for="batch">Enter batch</label>
						<input type="text" name="batch" id="batch" class="form-control" placeholder="13" 
							pattern="[0-9]{2}" required>
					</div>
					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-primary btn-block" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php require_once('footer.php'); ?>
</body>

</html>