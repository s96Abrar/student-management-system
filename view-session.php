<?php
	$title = 'Session';
	require_once('header.php');
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row">
			<div class="mt-2 pt-2 pr-3 text-justify">
				<h3>Session information</h3>
				<table class='table table-bordered table-condensed table-hover mt-3'>
					<thead class='thead-light text-center'>
						<tr>
							<th>Session</th>
							<th>Batch</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<?php
						$query = "SELECT * FROM session_info WHERE department_info_iddepartment_info=?";
						try {
							$stmt = $connection->prepare($query);
							$stmt->bind_param("i", $_SESSION['deptid']);
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($row = $result->fetch_assoc()) {
									echo "<tr><td>$row[session_name]</td><td>$row[session_batch]</td></tr>";
								}
							} else {
								echo "<tr colspan='2'>No session</tr>";
							}
							$stmt->close();
						} catch (Exception $ex) {

						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php require_once('footer.php'); ?>
</body>

</html>