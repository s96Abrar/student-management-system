<?php
	$title = "SignIn";
	require_once('header.php');

	console_log("At " . basename(__FILE__));
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">

<!-- Body -->
<div class="container">
	<div class="row justify-content-center align-items-center">
		<div class="mt-4 pt-4 pl-2 pr-3 text-justify">
			<div class="text-center">
				<h4>Sign in Student <br>Management System</h4>
			</div>
			<form class="shadow mt-3 p-5" action="welcome.php" method="POST">
				<div class="input-group mt-2 mb-2">
            <span class="input-group-prepend">
              <span class="input-group-text">
                <i class="fa fa-envelope fa-fw"></i>
              </span>
            </span>
					<input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter User Name"
								 autofocus required>
				</div>
				<div class="input-group mt-2 mb-2">
            <span class="input-group-prepend">
              <span class="input-group-text">
                <i class="fa fa-key fa-fw"></i>
              </span>
            </span>
					<input type="password" class="form-control" id="user_password" name="user_password"
								 placeholder="Enter Password" required>
				</div>
				<span class="small text-danger">
            <?php
							if (isset($_SESSION['errorval']) and !empty($_SESSION['errorval'])) {
								echo $_SESSION['errorval'];
								unset($_SESSION['errorval']);
							}
						?>
          </span>
				<button type="submit" class="btn btn-primary btn-block">Sign In</button>
			</form>
		</div>
	</div>
</div>

</body>

</html>
