<?php
// Setting variables for page
$title='Attendance';

require_once('header.php');
 ?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
  <!-- Navigation bar -->
  <?php require_once('navbar.php'); ?>

  <!-- Body -->
  <div class="container">
    <div class="row">
      <div class="mt-4 pt-4 pl-2 pr-3 text-justify">
        <h1 class="mt-4">Give attendance</h1>
        <p>Attendance system already made up. Just a link is needed.</p>
      </div>

      <!-- Footer -->
      <?php require_once('footer.php'); ?>
    </div>
  </div>
</body>

</html>
