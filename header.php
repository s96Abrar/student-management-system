<?php
	require_once('welcome.php')
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="application-name" content="Student Management System">
	<meta name="keywords" content="HTML,CSS,JS,PHP,Management,Student,System,University,Webapp">
	<meta name="description" content="Managing student information">
	<meta name="author" content="Abrar">

	<!-- Page Icon -->
	<link rel="icon" href="studentManagement.png">

	<!-- Bootstrap 5 CSS -->
	<!-- <link href="css/bootstrap.min.new.css" rel="stylesheet"> -->

	<!-- Bootstrap CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<!-- Font awesome fonts and icons CSS -->
	<link href="css/all.min.css" rel="stylesheet">
	<link href="css/fontawesome.min.css" rel="stylesheet">
	<link href="css/regular.min.css" rel="stylesheet">
	<link href="css/solid.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="css/style.css" rel="stylesheet">

	<!-- JQuery JS -->
	<script src="js/jquery.min.js"></script>

	<!-- Bootstrap 5 JS -->
	<!-- <script src="js/bootstrap.bundle.min.new.js"></script> -->

	<!-- Bootstrap JS -->
	<script src="js/bootstrap.bundle.min.js"></script>

	<!-- Custom JS -->
	<script src="js/customjs.js"></script>

	<title>Student Management System <?php if (isset($title)) echo '| ' . $title; ?></title>

</head>
