-- MySQL Script generated by MySQL Workbench
-- Sun 26 Sep 2021 08:55:56 PM +06
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema smsdb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `smsdb` ;

-- -----------------------------------------------------
-- Schema smsdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `smsdb` ;
USE `smsdb` ;

-- -----------------------------------------------------
-- Table `smsdb`.`department_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`department_info` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`department_info` (
  `iddepartment_info` INT NOT NULL AUTO_INCREMENT,
  `department_name` VARCHAR(60) NOT NULL,
  `department_short_name` VARCHAR(15) NULL,
  PRIMARY KEY (`iddepartment_info`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsdb`.`officers_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`officers_info` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`officers_info` (
  `idofficers_info` INT NOT NULL AUTO_INCREMENT,
  `officers_id` VARCHAR(30) NOT NULL,
  `officers_name` VARCHAR(60) NOT NULL,
  `officers_password` VARCHAR(56) NOT NULL,
  `officers_contact` VARCHAR(11) NOT NULL,
  `officers_email` VARCHAR(60) NULL,
  `department_info_iddepartment_info` INT NOT NULL,
  PRIMARY KEY (`idofficers_info`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsdb`.`students_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`students_info` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`students_info` (
  `idstudents_info` INT NOT NULL AUTO_INCREMENT,
  `students_roll` VARCHAR(15) NOT NULL,
  `students_name` VARCHAR(60) NOT NULL,
  `students_session` VARCHAR(10) NOT NULL,
  `students_email` VARCHAR(60) NOT NULL,
  `students_address` VARCHAR(200) NULL,
  `students_contact` VARCHAR(11) NOT NULL,
  `students_parent_contact` VARCHAR(11) NOT NULL,
  `students_date_of_birth` DATE NULL,
  `students_gender` ENUM('Male', 'Female') NOT NULL,
  `department_info_iddepartment_info` INT NOT NULL,
  PRIMARY KEY (`idstudents_info`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsdb`.`teachers_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`teachers_info` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`teachers_info` (
  `idteachers_info` INT NOT NULL AUTO_INCREMENT,
  `teachers_id` VARCHAR(45) NOT NULL,
  `teachers_name` VARCHAR(60) NOT NULL,
  `teachers_designation` ENUM('Lecturer', 'Assistant Professor', 'Associate Professor', 'Professor') NOT NULL,
  `teachers_mobile` VARCHAR(11) NOT NULL,
  `teachers_email` VARCHAR(60) NOT NULL,
  `department_info_iddepartment_info` INT NOT NULL,
  PRIMARY KEY (`idteachers_info`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsdb`.`course_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`course_info` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`course_info` (
  `idcourse_info` INT NOT NULL AUTO_INCREMENT,
  `course_code` VARCHAR(20) NOT NULL,
  `course_title` VARCHAR(60) NOT NULL,
  `course_type` ENUM('Theory', 'Lab', 'Project', 'Viva', 'Thesis', 'Internship') NOT NULL,
  `course_credit` FLOAT NOT NULL,
  `course_label` VARCHAR(45) NOT NULL,
  `department_info_iddepartment_info` INT NOT NULL,
  PRIMARY KEY (`idcourse_info`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsdb`.`activity_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`activity_history` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`activity_history` (
  `idactivity_history` INT NOT NULL AUTO_INCREMENT,
  `activity_time_date` TIMESTAMP NULL DEFAULT now(),
  `activity_description` VARCHAR(200) NOT NULL,
  `department_info_iddepartment_info` INT NOT NULL,
  PRIMARY KEY (`idactivity_history`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsdb`.`session_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`session_info` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`session_info` (
  `idsession_info` INT NOT NULL AUTO_INCREMENT,
  `session_name` VARCHAR(10) NOT NULL,
  `session_batch` INT NULL,
  `department_info_iddepartment_info` INT NOT NULL,
  PRIMARY KEY (`idsession_info`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsdb`.`ctexams_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`ctexams_info` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`ctexams_info` (
  `idctexams_info` INT NOT NULL AUTO_INCREMENT,
  `ctexams_name` VARCHAR(100) NOT NULL,
  `ctexams_session` VARCHAR(10) NOT NULL,
  `ctexams_year` ENUM('1', '2', '3', '4') NOT NULL,
  `ctexams_term` ENUM('1', '2') NOT NULL,
  `ctexams_table_name` VARCHAR(60) NOT NULL,
  `department_info_iddepartment_info` INT NOT NULL,
  `course_info_idcourse_info` INT NOT NULL,
  PRIMARY KEY (`idctexams_info`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsdb`.`routines_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`routines_info` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`routines_info` (
  `idroutines_info` INT NOT NULL AUTO_INCREMENT,
  `routines_year` ENUM('1', '2', '3', '4') NOT NULL,
  `routines_term` ENUM('1', '2') NOT NULL,
  `routines_table_name` VARCHAR(60) NOT NULL,
  `department_info_iddepartment_info` INT NOT NULL,
  PRIMARY KEY (`idroutines_info`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `smsdb`.`results_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smsdb`.`results_info` ;

CREATE TABLE IF NOT EXISTS `smsdb`.`results_info` (
  `idresults_info` INT NOT NULL AUTO_INCREMENT,
  `results_session` VARCHAR(10) NOT NULL,
  `results_year` ENUM('1', '2', '3', '4') NOT NULL,
  `results_term` ENUM('1', '2') NOT NULL,
  `results_table_name` VARCHAR(60) NOT NULL,
  `department_info_iddepartment_info` INT NOT NULL,
  PRIMARY KEY (`idresults_info`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
