<?php
	$title = 'Student';
	require_once('header.php');

	if (!post_check('selectedFileID')) {
		header('Location: student-list.php');
	}

?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
<!-- Navigation bar -->
<?php require_once('navbar.php'); ?>

<!-- Body -->
<div class="container">
	<div class="row">
		<div class="col mt-4 pt-4 pl-2 pr-3 mb-4 text-center">
			<h4>Student's Profile</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-3 justify-content-center">
			<img class="img-fluid" src="profile.png" alt="Student Pic">
			<!-- <a class="btn w-100" href="#"><i class="fa fa-pencil-alt"></i> Change Picture</a> -->
		</div>
		<div class="col mt-4">
			<?php
				$query = "SELECT * FROM students_info WHERE students_roll=?";
				try {
					$stmt = $connection->prepare($query);
					$stmt->bind_param("s", $_POST['selectedFileID']);
					$stmt->execute();
					$result = $stmt->get_result();
					$stmt->close();
					$row = $result->fetch_assoc();
				} catch (Exception $ex) {
				}
			?>
			<table class='table table-borderless'>
				<tbody>
				<tr>
					<td class="text-right"><i class="far fa-id-card fa-fw mr-2"></i>ID</td>
					<td><?php echo $row['students_roll']; ?></td>
				</tr>
				<tr>
					<td class="text-right"><i class="far fa-user fa-fw mr-2"></i>Name</td>
					<td><?php echo $row['students_name']; ?></td>
				</tr>
				<tr>
					<td class="text-right"><i class="far fa-calendar fa-fw mr-2"></i>Session</td>
					<td><?php echo $row['students_session']; ?></td>
				</tr>
				<tr>
					<td class="text-right"><i class="far fa-address-book fa-fw mr-2"></i>Mobile</td>
					<td><?php echo $row['students_contact']; ?></td>
				</tr>
				<tr>
					<td class="text-right"><i class="far fa-envelope fa-fw mr-2"></i>Email</td>
					<td><?php echo $row['students_email']; ?></td>
				</tr>
				<tr>
					<td class="text-right"><i class="far fa-venus-mars fa-fw mr-2"></i>Gender</td>
					<td><?php echo $row['students_gender']; ?></td>
				</tr>
				<tr>
					<td class="text-right"><i class="far fa-birthday-cake fa-fw mr-2"></i>Date of Birth</td>
					<td><?php echo $row['students_date_of_birth']; ?></td>
				</tr>
				<tr>
					<td class="text-right"><i class="far fa-venus-mars fa-fw mr-2"></i>Parent Contact</td>
					<td><?php echo $row['students_parent_contact']; ?></td>
				</tr>
				<tr>
					<td class="text-right"><i class="far fa-location-arrow fa-fw mr-2"></i>Address</td>
					<td><?php echo $row['students_address']; ?></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- Page Script -->
<script>
	// $(function() {
	//   $('#profile-image1').on('click', function() {
	//     $('#profile-image-upload').click();
	//   });
	// });
</script>

<!-- Footer -->
<?php require_once('footer.php'); ?>
</body>

</html>
