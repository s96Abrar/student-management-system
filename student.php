<?php
	// Setting variables for page
	$title = 'Student';

	require_once('header.php');
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
<!-- Navigation bar -->
<?php require_once('navbar.php'); ?>

<!-- Body -->
<div class="container">
	<div class="row">
		<div class="mt-4 pt-4 pl-2 pr-3 text-justify">
			<h1 class="mt-4">Manage students</h1>
			<div class="card-deck mt-4">
				<div class="card w-75 shadow rounded">
					<div class="card-header">
						<h5 class="card-title">Register new student</h5>
					</div>
					<div class="card-body">
						<p class="card-text">New student information will be entered into the database.</p>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col">
								<p class="card-text vertical-center">
									<small class="text-muted">
										<!-- No text -->
									</small>
								</p>
							</div>
							<div class="col text-right">
								<a href="student-registration.php" class="btn btn-primary">Go</a>
							</div>
						</div>
					</div>
				</div>

				<div class="card w-75 shadow rounded">
					<div class="card-header">
						<h5 class="card-title">Student List</h5>
					</div>
					<div class="card-body">
						<p class="card-text">All the student information is listed here.</p>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col">
								<p class="card-text vertical-center">
									<small class="text-muted">
										<!-- No text -->
									</small>
								</p>
							</div>
							<div class="col text-right">
								<a href="student-list.php" class="btn btn-primary">Go</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Footer -->
<?php require_once('footer.php'); ?>
</body>

</html>
