<?php
	function createNavItem($title, $titleTxt, $linkTxt, $dropdownItems = "")
	{
		$itemTxt = '';
		$activeTxt = '';
		$tmpLink = $linkTxt;

		if (isset($title)) {
			if ($title === $titleTxt) {
				$activeTxt = "active";
				$tmpLink = '';
			}
		}

		$itemTxt .= "<li class='nav-item $activeTxt ";

		if ($dropdownItems and is_array($dropdownItems)) {
			$itemTxt .= "dropdown'><a class='nav-link dropdown-toggle' href='$tmpLink' id=navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
			$itemTxt .= $titleTxt . "</a>";
			$dropdownTxt = "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";

			// key = link, value = text
			foreach ($dropdownItems as $key => $value) {
				$dropdownTxt .= "<a class='dropdown-item' href='$key'>$value</a>";
			}
			$dropdownTxt .= "</div>";
			$itemTxt .= $dropdownTxt;
			$itemTxt .= "</li>";
		} else {
			$itemTxt .= "'><a class='nav-link' href='$tmpLink'>$titleTxt</a></li>";
		}

		echo $itemTxt;
	}

?>

<nav class="navbar navbar-expand-md navbar-light bg-light border-bottom fixed-top">
	<div class="container">
		<a class="ml-2 navbar-brand page-scroll" href="#page-top">
			<img src="studentManagement.png" alt="Student Management System" height="48px" width="48px">
			<label class="brand-name sr-only">Student Management System</label>
		</a>

		<button id="menu-toggle" class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
						aria-label="Toggle navigation">
			<span id="changeToggleIcon" class="navbar-toggler-icon"></span>
		</button>

		<?php
			if (!isset($title)) {
				$title = '';
			}
		?>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
				<!-- Dashboard -->
				<?php createNavItem($title, 'Dashboard', 'index.php'); ?>
				<!-- Teacher -->
				<?php createNavItem($title, 'Teacher', 'teacher.php'); ?>
				<!-- Session -->
				<?php createNavItem($title, 'Session', '', ['add-session.php' => 'Add new session', 'view-session.php' => 'View session info']); ?>
				<!-- Student -->
				<?php createNavItem($title, 'Student', '', ['student-registration.php' => 'Register new student', 'student-list.php' => 'Student list']); ?>
				<!-- CT Exam -->
				<?php createNavItem($title, 'CT Exam', '', ['add-ct-result.php' => 'Add CT Exam result', 'view-ct-result.php' => 'View CT Exam result']); ?>
				<!-- Results -->
				<?php createNavItem($title, 'Final Result', '', ['add-final-result.php' => 'Add final result', 'view-final-result.php' => 'View final result']); ?>
				<!-- Class Routine -->
				<?php createNavItem($title, 'Class Routine', '', ['add-routine.php' => 'Add new routine', 'view-routine.php' => 'See existing routine']); ?>
				<!-- Profile -->
				<?php
					$isuser = isset($_SESSION['officername']);
					$userName = '';
					if ($isuser) {
						$user = $_SESSION['officername'];
						if (!empty($user)) {
							$userName = $user;
							unset($user);
						} else $userName = "Profile";
					} else $userName = "Profile";
					unset($isuser);

					createNavItem('', $userName, '', ['signout.php' => 'Sign out']);
				?>
			</ul>
		</div>
	</div>
</nav>