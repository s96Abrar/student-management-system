<?php
// Setting variables for page
$title='Assignment';

require_once('header.php');
 ?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
  <!-- Navigation bar -->
  <?php require_once('navbar.php'); ?>

  <!-- Body -->
  <div class="container">
    <div class="row">
      <div class="mt-4 pt-4 pl-2 pr-3 text-justify">
        <h1 class="mt-4">Assignment</h1>
        <p>No use case.</p>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <?php require_once('footer.php'); ?>
</body>

</html>
