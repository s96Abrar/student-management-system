<?php
	// Setting variables for page
	$title = 'CT Result';

	require_once('header.php');
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row">
			<div class="col mt-2 pt-2 pr-3 text-justify">
				<h1>CT Result</h1>
				<p>See CT exams results</p>
				<div class="container mt-5">
					<form action="#result_table" method="post">
						<div class="row">
							<div class="form-group ml-2 mr-2">
								<label for="session">Select Session</label>
								<select class="form-control custom-select" name="session" id="session" onchange="updateCourseList();" required>
									<option value="" selected disabled hidden>None</option>
									<?php
										$query = "SELECT session_name FROM session_info WHERE department_info_iddepartment_info=? ORDER BY session_name";
										try {
											$stmt = $connection->prepare($query);
											$stmt->bind_param("i", $_SESSION['deptid']);
											$stmt->execute();
											$result = $stmt->get_result();
											while ($row = $result->fetch_row()) {
												echo "<option value='$row[0]'>$row[0]</option>";	
											}
											$stmt->close();
										} catch (Exception $ex) {}
									?>
								</select>
							</div>
							<div class="form-group ml-2 mr-2">
								<label for="term">Select Term</label>
								<select class="form-control custom-select" name="term" id="term" onchange="updateCourseList();" required>
									<option value="" selected disabled hidden>None</option>
									<option value="1">1</option>
									<option value="2">2</option>
								</select>
							</div>
							<div class="form-group ml-2 mr-2">
								<label for="course_code">Select Couese Code</label>
								<select class="form-control custom-select" name="course_code" id="course_code" required>
									<option value="" selected disabled hidden>None</option>
									<!-- Populate course code by slection change event of session and term -->
									<!-- <option value="CSE3101">CSE3101</option> -->
									<!-- <option value="CSE3103">CSE3103</option> -->
									<!-- <option value="CSE3104">CSE3104</option> -->
									<!-- <option value="CSE3105">CSE3105</option> -->
									<!-- <option value="CSE3106">CSE3106</option> -->
								</select>
							</div>
						</div>

						<div class="form-group">
							<button type="submit" name="submitSessionTermCourse" id="submitSessionTermCourse"
								class="btn btn-primary">Submit Query
							</button>
						</div>
					</form>
				</div>

				<div id="result_table" class="container mt-5">
					<?php
					if (isset($_POST['session']) and isset($_POST['term']) and isset($_POST['course_code'])) {
						?>
					<div class="row">
						<table class="table table-stripped table-bordered">
							<thead class="thead-light">
								<?php
									$query = "SELECT * FROM ctexams_info WHERE ctexams_session=? AND ctexams_term=? AND department_info_iddepartment_info=? AND course_info_idcourse_info=?";
									try {
										$stmt = $connection->prepare($query);
										$stmt->bind_param("ssii", $_POST['session'], $_POST['term'], $_SESSION['deptid'], $_POST['course_code']);
										$stmt->execute();
										$result = $stmt->get_result();
										$num_rows = $result->num_rows;
										$stmt->close();

										$found = FALSE;

										if ($num_rows > 0) {
											$found = TRUE;

											$row = $result->fetch_assoc();
											$filename = $row['ctexams_table_name'];
											$filepath = 'uploads/ctexams/' . $filename;
											$temp = explode('.', $filename);
											$ext = end($temp);

											if ($ext === 'csv') {
												if (($file = fopen($filepath, 'r')) !== FALSE) {
													set_time_limit(0);
													$sep = ',';

													$firstline = fgets($file, 4096);
													//Gets the number of fields, in CSV-files the names of the fields are mostly given in the first line
													$num = strlen($firstline) - strlen(str_replace($sep, "", $firstline));

													//save the different fields of the firstline in an array called fields
													$fields = array();
													$fields = explode($sep, $firstline, ($num + 1));

													$line = array();
													$i = 0;
													$dsatz = array();

													//CSV: one line is one record and the cells/fields are seperated by ";"
													//so $dsatz is an two dimensional array saving the records like this: $dsatz[number of record][number of cell]
													while ($line[$i] = fgets($file, 4096)) {
														$dsatz[$i] = array();
														$dsatz[$i] = explode($sep, $line[$i], ($num + 1));

														$i++;
													}

													echo "<tr>";
													for ($k = 0; $k != ($num + 1); $k++) {
														echo "<th>" . $fields[$k] . "</th>";
													}
													echo "</tr>";
													echo "</thead>";
													echo "<tbody>";
													foreach ($dsatz as $key => $number) {
														//new table row for every record
														echo "<tr>";
														foreach ($number as $k => $content) {
															//new table cell for every field of the record
															echo "<td>" . $content . "</td>";
														}
														echo "</tr>";
													}
													echo "</tbody>";

													fclose($file);
												} else {
													echo 'cannot open file';
													echo "</thead>";
												}
											} else {
												echo "</thead>";
											}
										}

										if (!$found) {
											echo "<tr><th class='text-center'>No result found.</th></tr></thead>";
										}
									} catch (Exception $ex) {
										echo $ex->getMessage();
										echo "<tr>ERROR!</tr></thead>";
									}
								?>
						</table>
					</div>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php require_once('footer.php'); ?>

	<script>
		function updateCourseList() {
			var session = $("#session").val();
			var term = $("#term").val();

			var courseList = "<option value='' selected disabled hidden>None</option>";

			if (session === null || term === null) {
				$("#course_code").html(courseList);
				return;
			}

			if (session === "2017-2018") {
				if (term === "1") {
					courseList += "<option value='42'>CSE3103</option>";
					courseList += "<option value='43'>CSE3104</option>";
					courseList += "<option value='44'>CSE3105</option>";
					courseList += "<option value='45'>CSE3106</option>";
					courseList += "<option value='46'>BUS3107</option>";
					courseList += "<option value='47'>BUS3108</option>";
				} else {
					courseList += "<option value='51'>CSE3201</option>";
				}
			} else if (session === "2018-2019") {
				if (term === "1") {
					courseList += "<option value='20'>CSE2101</option>";
					courseList += "<option value='21'>CSE2102</option>";
					courseList += "<option value='24'>CSE2105</option>";
					courseList += "<option value='25'>CSE2106</option>";
				} else {
					courseList += "<option value='31'>CSE2201</option>";
					courseList += "<option value='32'>CSE2202</option>";
					courseList += "<option value='34'>CSE2205</option>";
					courseList += "<option value='35'>CSE2206</option>";
					courseList += "<option value='36'>CSE2207</option>";
					courseList += "<option value='37'>CSE2208</option>";
					courseList += "<option value='40'>BUS2211</option>";
				}
			} else if (session === "2019-2020") {
				if (term === "1") {
					courseList += "<option value='1'>CSE1101</option>";
					courseList += "<option value='2'>CSE1102</option>";
					courseList += "<option value='3'>CSE1103</option>";
				} else {
					courseList += "<option value='10'>CSE1201</option>";
					courseList += "<option value='11'>CSE1202</option>";
					courseList += "<option value='12'>CSE1203</option>";
					courseList += "<option value='13'>CSE1204</option>";
				}
			}

			$("#course_code").html(courseList);
		}
	</script>
</body>

</html>
