<?php
	$title = 'Class Routine';
	require_once('header.php');

	if (post_check('submit') and isset($_FILES['csv']) and $_FILES['csv']['error'] === 0 and post_check('term') and post_check('year')) {
		$file = $_FILES['csv']['name'];
		$filesize = $_FILES['csv']['size'];
		$filename = basename($file);
		$temp = explode('.', $filename);
		$ext = end($temp);
		$tmpName = $_FILES['csv']['tmp_name'];

		$sessdata = array();

		if (preg_match('/^[^.][-a-z0-9_.]+[a-z]$/i', $file)) {
			if ($ext === 'csv') {
				if ($filesize < 10000000) { // 10MB
					$newName = 'routines_' . $_POST['year'] . '_' . $_POST['term'] . '.csv';

					$query = "SELECT * FROM routines_info WHERE routines_year=? AND routines_term=? AND department_info_iddepartment_info=?";
					try {
						$stmt = $connection->prepare($query);
						$stmt->bind_param("ssi", $_POST['year'], $_POST['term'], $_SESSION['deptid']);
						$stmt->execute();
						$num_rows = $stmt->get_result()->num_rows;
						$stmt->close();

						$done = FALSE;

						if ($num_rows > 0) {
							$query = "UPDATE routines_info SET routines_table_name=? WHERE routines_year=? AND routines_term=? AND department_info_iddepartment_info=?";
							$stmt = $connection->prepare($query);
							$stmt->bind_param("sssi", $newName, $_POST['year'], $_POST['term'], $_SESSION['deptid']);
							$update = $stmt->execute();
							$stmt->close();

							if ($update) {
								$done = TRUE;
								$sessdata['type'] = 'success';
								$sessdata['message'] = 'Replaced old routine.';
							} else {
								$done = FALSE;
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Cannot replace old routine';
							}
						} else {
							$query = "INSERT INTO routines_info VALUES (NULL, ?, ?, ?, ?)";
							$stmt = $connection->prepare($query);
							$stmt->bind_param("sssi", $_POST['year'], $_POST['term'], $newName, $_SESSION['deptid']);
							$insert = $stmt->execute();
							$stmt->close();

							if ($insert) {
								$query = "INSERT INTO activity_history (activity_description, department_info_iddepartment_info) VALUES (?, ?)";
								$stmt = $connection->prepare($query);
								$desc = "Added new routine for year " . $_POST['year'] . " term " . $_POST['term'];
								$stmt->bind_param("si", $desc, $_SESSION['deptid']);
								$stmt->execute();
								$stmt->close();

								$done = TRUE;
								$sessdata['type'] = 'success';
								$sessdata['message'] = 'Routine added successfully';
							} else {
								$done = FALSE;
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Cannot add routine';
							}
						}

						if ($done) {
							copy($tmpName, 'uploads/routines/' . $newName);
						}
					} catch (Exception $ex) {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Query error';
					}
				} else {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'File size is more than 10MB';
				}
			} else {
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Invalid file type provided.';
			}
		} else {
			$sessdata['type'] = 'danger';
			$sessdata['message'] = 'Invalid file!!!';
		}

		$_SESSION['sessdata'] = $sessdata;
		unset($_POST);
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row">
			<div class="col mt-2 pt-2 pr-3 text-justify">
				<h1>Class Routine</h1>
				<p>Add routine or replace old one.</p>
				<form class="mt-3" action="" method="post" enctype="multipart/form-data">
					<div class="form-group ml-3">
						<label for="csv">Upload your routine as csv</label>
						<input type="file" class="form-control border-0" name="csv" id="csv" required>
					</div>
					<div class="row ml-2">
						<div class="form-group ml-2 mr-2">
							<label for="year">Select year</label>
							<select class="form-control custom-select" name="year" id="year" required>
								<option value="" selected disabled hidden>Year</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="form-group ml-2 mr-2">
							<label for="term">Select Term</label>
							<select class="form-control custom-select" name="term" id="term" required>
								<option value="" selected disabled>Term</option>
								<option value="1">1</option>
								<option value="2">2</option>
							</select>
						</div>

					</div>
					<div class="row ml-2 mt-4 mb-2">
						<div class="form-group ml-2 mr-2">
							<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Upload routine">
						</div>
						<div class="form-group mb-3 ml-2 mr-2">
							<a href="download.php?link=uploads/routines/template_routine.csv" target="_blank"
								class="btn btn-secondary" name="downloadTemplate">Download Template CSV</a>
						</div>
					</div>
					
					<span id="status"><b>N.B. If a routine already exists then it will replace the old one.</b></span>
					<?php
							if (isset($_SESSION['sessdata'])) {
								$sessdata = $_SESSION['sessdata'];
								?>
					<div class="row mt-2 pt-2 pl-2">
						<span class="text-<?php echo $sessdata['type']; ?>">
							<b><?php echo $sessdata['message']; ?></b>
						</span>
					</div>
					<?php
								unset($_SESSION['sessdata']);
							}
						?>
				</form>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php require_once('footer.php'); ?>
</body>

</html>