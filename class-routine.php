<?php
	// Setting variables for page
	$title = 'Class Routine';

	require_once('header.php');
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
<!-- Navigation bar -->
<?php require_once('navbar.php'); ?>

<!-- Body -->
<div class="container">
	<div class="row">
		<div class="mt-4 pt-4 pl-2 pr-3 text-justify">
			<h1 class="mt-4">Class Routine</h1>
			<div class="card-deck mt-4">
				<div class="card w-75 shadow rounded">
					<div class="card-header">
						<h5 class="card-title">Add new routine</h5>
					</div>
					<div class="card-body">
						<p class="card-text">Assign new routine or replace old routine.</p>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col text-right">
								<a href="add-routine.php" class="btn btn-primary">Go</a>
							</div>
						</div>
					</div>
				</div>

				<div class="card w-75 shadow rounded">
					<div class="card-header">
						<h5 class="card-title">See routine</h5>
					</div>
					<div class="card-body">
						<p class="card-text">See existing routine.</p>
					</div>
					<div class="card-footer">
						<div class="row">
							<div class="col text-right">
								<a href="old-routine.php" class="btn btn-primary">Go</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Footer -->
<?php require_once('footer.php'); ?>
</body>

</html>
