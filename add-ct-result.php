<?php
	// Setting variables for page
	$title = 'CT Result';

	require_once('header.php');

	if (post_check('submit') and isset($_FILES['csv']) and 
			$_FILES['csv']['error'] === 0 and post_check('session') and 
			post_check('term') and post_check('course_code')) {

		$year = "4";
		if ($_POST['session'] === "2017-2018") {
			$year = "3";
		} else if ($_POST['session'] === "2018-2019") {
			$year = "2";
		} else if ($_POST['session'] === "2017-2018") {
			$year = "1";
		}

		$file = $_FILES['csv']['name'];
		$filesize = $_FILES['csv']['size'];
		$filename = basename($file);
		$temp = explode('.', $filename);
		$ext = end($temp);
		$tmpName = $_FILES['csv']['tmp_name'];

		$sessdata = array();

		if (preg_match('/^[^.][-a-z0-9_.]+[a-z]$/i', $file)) {
			if ($ext === 'csv') {
				if ($filesize < 10000000) { // 10MB
					$newName = 'ctexams_' . $_POST['session'] . '_' . $_POST['term'] . '_' . $_POST['course_code'] . '.csv';

					$query = "SELECT * FROM ctexams_info WHERE ctexams_session=? AND ctexams_year=? AND ctexams_term=? AND department_info_iddepartment_info=? AND course_info_idcourse_info=?";
					try {
						$stmt = $connection->prepare($query);
						$stmt->bind_param("sssii", $_POST['session'], $year, $_POST['term'], $_SESSION['deptid'], $_POST['course_code']);
						$stmt->execute();
						$num_rows = $stmt->get_result()->num_rows;
						$stmt->close();

						$done = FALSE;

						if ($num_rows > 0) {
							$query = "UPDATE ctexams_info SET ctexams_table_name=? WHERE ctexams_session=? AND ctexams_year=? AND ctexams_term=? AND department_info_iddepartment_info=? AND course_info_idcourse_info=?";
							$stmt = $connection->prepare($query);
							$stmt->bind_param("ssssii", $newName, $_POST['session'], $year, $_POST['term'], $_SESSION['deptid'], $_POST['course_code']);
							$update = $stmt->execute();
							$stmt->close();

							if ($update) {
								$done = TRUE;
								$sessdata['type'] = 'success';
								$sessdata['message'] = 'Replaced old result.';
							} else {
								$done = FALSE;
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Cannot replace old result';
							}
						} else {
							$ctexam_name = "CTExam";
							$query = "INSERT INTO ctexams_info VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)";
							$stmt = $connection->prepare($query);
							$stmt->bind_param("sssssii", $ctexam_name, $_POST['session'], $year, $_POST['term'], $newName, $_SESSION['deptid'], $_POST["course_code"]);
							$insert = $stmt->execute();
							$stmt->close();

							if ($insert) {
								$query = "INSERT INTO activity_history (activity_description, department_info_iddepartment_info) VALUES (?, ?)";
								$stmt = $connection->prepare($query);
								$desc = "Added CT exam result for " . $_POST['session'];
								$stmt->bind_param("si", $desc, $_SESSION['deptid']);
								$stmt->execute();
								$stmt->close();

								$done = TRUE;
								$sessdata['type'] = 'success';
								$sessdata['message'] = 'Result added successfully';
							} else {
								$done = FALSE;
								$sessdata['type'] = 'danger';
								$sessdata['message'] = 'Cannot add Result';
							}
						}

						if ($done) {
							copy($tmpName, 'uploads/ctexams/' . $newName);
						}
					} catch (Exception $ex) {
						$sessdata['type'] = 'danger';
						$sessdata['message'] = 'Query error';
					}
				} else {
					$sessdata['type'] = 'danger';
					$sessdata['message'] = 'File size is more than 10MB';
				}
			} else {
				$sessdata['type'] = 'danger';
				$sessdata['message'] = 'Invalid file type provided.';
			}
		} else {
			$sessdata['type'] = 'danger';
			$sessdata['message'] = 'Invalid file!!!';
		}

		$_SESSION['sessdata'] = $sessdata;
		unset($_POST);
	}
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
	<!-- Navigation bar -->
	<?php require_once('navbar.php'); ?>

	<!-- Body -->
	<div class="container">
		<div class="row">
			<div class="col mt-2 pt-2 pr-3 text-justify">
				<h1>CT Result</h1>
				<p>Add CT exam result</p>
				<div class="container mt-5">
					<form class="mt-3" action="" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="csv">Upload your routine as csv</label>
							<input type="file" class="form-control border-0" name="csv" id="csv" required>
						</div>
						<div class="row">
							<div class="form-group ml-2 mr-2">
								<label for="session">Select Session</label>
								<select class="form-control custom-select" name="session" id="session" onchange="updateCourseList();" required>
									<option value="" selected disabled hidden>None</option>
									<?php
										$query = "SELECT session_name FROM session_info WHERE department_info_iddepartment_info=? ORDER BY session_name";
										try {
											$stmt = $connection->prepare($query);
											$stmt->bind_param("i", $_SESSION['deptid']);
											$stmt->execute();
											$result = $stmt->get_result();
											while ($row = $result->fetch_row()) {
												echo "<option value='$row[0]'>$row[0]</option>";	
											}
											$stmt->close();
										} catch (Exception $ex) {}
									?>
								</select>
							</div>
							<div class="form-group ml-2 mr-2">
								<label for="term">Select Term</label>
								<select class="form-control custom-select" name="term" id="term" onchange="updateCourseList();" required>
									<option value="" selected disabled hidden>None</option>
									<option value="1">1</option>
									<option value="2">2</option>
								</select>
							</div>
							<div class="form-group ml-2 mr-2">
								<label for="course_code">Select Couese Code</label>
								<select class="form-control custom-select" name="course_code" id="course_code" required>
									<option value="" selected disabled hidden>None</option>
									<!-- Populate course code by slection change event of session and term -->
									<!-- <option value="CSE3101">CSE3101</option> -->
									<!-- <option value="CSE3103">CSE3103</option> -->
									<!-- <option value="CSE3104">CSE3104</option> -->
									<!-- <option value="CSE3105">CSE3105</option> -->
									<!-- <option value="CSE3106">CSE3106</option> -->
								</select>
							</div>
						</div>

						<div class="row mt-4 mb-2">
							<div class="form-group ml-2 mr-2">
								<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Upload CT Result">
							</div>
							<div class="form-group mb-3 ml-2 mr-2">
								<a href="download.php?link=uploads/ctexams/template_ctexam.csv" target="_blank"
									class="btn btn-secondary" name="downloadTemplate">Download Template CSV</a>
							</div>
						</div>

						<span id="status"><b>N.B. If a result sheet already exists then it will replace the old one.</b></span>
						<?php
							if (isset($_SESSION['sessdata'])) {
								$sessdata = $_SESSION['sessdata'];
								?>
						<div class="row mt-2 pt-2 pl-2">
							<span class="text-<?php echo $sessdata['type']; ?>">
								<b><?php echo $sessdata['message']; ?></b>
							</span>
						</div>
						<?php
								unset($_SESSION['sessdata']);
							}
						?>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php require_once('footer.php'); ?>

	<script>
		function updateCourseList() {
			var session = $("#session").val();
			var term = $("#term").val();

			var courseList = "<option value='' selected disabled hidden>None</option>";

			if (session === null || term === null) {
				$("#course_code").html(courseList);
				return;
			}

			if (session === "2017-2018") {
				if (term === "1") {
					courseList += "<option value='42'>CSE3103</option>";
					courseList += "<option value='43'>CSE3104</option>";
					courseList += "<option value='44'>CSE3105</option>";
					courseList += "<option value='45'>CSE3106</option>";
					courseList += "<option value='46'>BUS3107</option>";
					courseList += "<option value='47'>BUS3108</option>";
				} else {
					courseList += "<option value='51'>CSE3201</option>";
				}
			} else if (session === "2018-2019") {
				if (term === "1") {
					courseList += "<option value='20'>CSE2101</option>";
					courseList += "<option value='21'>CSE2102</option>";
					courseList += "<option value='24'>CSE2105</option>";
					courseList += "<option value='25'>CSE2106</option>";
				} else {
					courseList += "<option value='31'>CSE2201</option>";
					courseList += "<option value='32'>CSE2202</option>";
					courseList += "<option value='34'>CSE2205</option>";
					courseList += "<option value='35'>CSE2206</option>";
					courseList += "<option value='36'>CSE2207</option>";
					courseList += "<option value='37'>CSE2208</option>";
					courseList += "<option value='40'>BUS2211</option>";
				}
			} else if (session === "2019-2020") {
				if (term === "1") {
					courseList += "<option value='1'>CSE1101</option>";
					courseList += "<option value='2'>CSE1102</option>";
					courseList += "<option value='3'>CSE1103</option>";
				} else {
					courseList += "<option value='10'>CSE1201</option>";
					courseList += "<option value='11'>CSE1202</option>";
					courseList += "<option value='12'>CSE1203</option>";
					courseList += "<option value='13'>CSE1204</option>";
				}
			}

			$("#course_code").html(courseList);
		}
	</script>
</body>

</html>