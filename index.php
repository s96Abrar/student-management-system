<?php
	// Setting variables for page before calling header
	$title = 'Dashboard';

	require_once('header.php');

	console_log("At " . basename(__FILE__));

?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
<!-- Navigation bar -->
<?php require_once('navbar.php'); ?>

<!-- Body -->
<div class="container">
	<div class="row">
		<div class="mt-2 pt-2 pr-3 text-justify">
			<h1>Welcome</h1>
			<p>You can manage student information and many more in here.</p>
			<div class="mt-4">
				<h4><b>Recent Activities</b></h4>
				<div class="list-group">
					<?php
						$query = "SELECT * FROM activity_history WHERE department_info_iddepartment_info=? ORDER BY activity_time_date DESC";
						try {
							$stmt = $connection->prepare($query);
							$stmt->bind_param("i", $_SESSION['deptid']);
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($row = $result->fetch_assoc()) {
									?>
									<a class='list-group-item list-group-item-action bg-light'>
										<?php echo $row['activity_time_date'] . ' - ' . $row['activity_description'];?>
									</a>
									<?php
								}
							} else {
								?>
								<a class='list-group-item list-group-item-action bg-light'>No Activity.</a>
								<?php
							}
							$stmt->close();
						} catch (Exception $ex) {
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Footer -->
<?php require_once('footer.php'); ?>
</body>

</html>
