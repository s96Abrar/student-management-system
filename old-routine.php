<?php
require_once('header.php');
 ?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
  <!-- Navigation bar -->
  <?php require_once('navbar.php'); ?>

  <!-- Body -->
  <div class="container">
    <div class="row">
      <div class="mt-4 pt-4 pl-2 pr-3 text-justify">
        <h1 class="mt-4">Class Routine</h1>
        <p>View previous routines.</p>

        <div class="container mt-5">
          <div class="row">
            <form class="row" action="#routine" method="post">
              <div class="form-group col">
                <label for="session">Select Session</label>
                <select class="custom-select" name="session" id="session" required>
                  <option value="" selected disabled>Session</option>
                  <option value="2017-2018">2017-2018</option>
                  <option value="2018-2019">2018-2019</option>
                  <option value="2019-2020">2019-2020</option>
                </select>
              </div>
              <div class="form-group col">
                <label for="term">Select Term</label>
                <select class="custom-select" name="term" id="term" required>
                  <option value="" selected disabled>Term</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                </select>
              </div>

              <div class="form-group col mt-2">
                <button type="button" name="submitSessionTerm" id="submitSessionTerm" class="btn btn-primary mt-4">Submit Query</button>
              </div>
            </form>
          </div>
        </div>

        <div id="routine" class="container mt-5">
          <div class="row">
            <p>Show routine table or image.</p>
            <table class="table table-stripped table-bordered">
              <!-- Populate thead and tbody from database -->
              <thead class="thead-light">
                <tr>
                  <th>Day</th>
                  <th>09:00AM-11:00AM</th>
                  <th>11:00AM-01:00PM</th>
                  <th>01:00PM-02:00PM</th>
                  <th>02:00PM-04:00PM</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Sunday</td>
                  <td>Course Code: teacher name</td>
                  <td>Course Code: teacher name</td>
                  <td rowspan="5" width="2%">B<br><br>R<br><br>E<br><br>A<br><br>K</td>
                  <td>Course Code: teacher name</td>
                </tr>
                <tr>
                  <td>Monday</td>
                  <td>Course Code: teacher name</td>
                  <td>Course Code: teacher name</td>
                  <!-- <td>R</td> -->
                  <td>Course Code: teacher name</td>
                </tr>
                <tr>
                  <td>Tuesday</td>
                  <td>Course Code: teacher name</td>
                  <td>Course Code: teacher name</td>
                  <!-- <td>E</td> -->
                  <td>Course Code: teacher name</td>
                </tr>
                <tr>
                  <td>Sunday</td>
                  <td>Course Code: teacher name</td>
                  <td>Course Code: teacher name</td>
                  <!-- <td>A</td> -->
                  <td>Course Code: teacher name</td>
                </tr>
                <tr>
                  <td>Sunday</td>
                  <td>Course Code: teacher name</td>
                  <td>Course Code: teacher name</td>
                  <!-- <td>K</td> -->
                  <td>Course Code: teacher name</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <?php require_once('footer.php'); ?>
</body>

</html>
