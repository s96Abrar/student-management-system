<?php
	require_once('dbconn.php');

	function console_log($data)
	{
		echo '<script>';
		echo 'console.log(' . json_encode($data) . ')';
		echo '</script>';
	}

	function post_check($var)
	{
		if (isset($_POST[$var]) and !empty($_POST[$var])) {
			return true;
		}
		return false;
	}

	function checkUser(&$connection, &$uname, &$upass)
	{
		if (empty($uname) || empty($upass)) {
			return "Empty username and password";
		}

		$safe_id = mysqli_real_escape_string($connection, $uname);
		$safe_pass = mysqli_real_escape_string($connection, $upass);
		$query = "SELECT officers_id, officers_name, officers_password, department_info_iddepartment_info FROM officers_info WHERE officers_id = '" . $safe_id . "'";
		$data = mysqli_query($connection, $query);

		$safe_id = NULL;
		$query = NULL;

		if (mysqli_num_rows($data) > 0) {
			while ($row = $data->fetch_row()) {
				if (md5($safe_pass) == $row[2]) {
					$_SESSION['deptid'] = $row[3];
					$_SESSION['officername'] = $row[1];
					$_SESSION['username'] = $row[0];
					return "";
				}
			}
		} else {
			return "No user found with that name";
		}

		return "Password not matched";
	}

	console_log("Checking value at " . basename(__FILE__));

	session_start();
	console_log("Session started...");

	// Page request from SignIn page if user_name and user_password contains
	console_log("Checking POST values...");
	if (!isset($_POST['user_name']) and !isset($_POST['user_password'])) {
		console_log("POST value not found.");
	} else {
		$getuser = $_POST['user_name'];
		$getpass = $_POST['user_password'];

		console_log("Setting username...");
		$retval = checkUser($connection, $getuser, $getpass);
		if (empty($retval)) { // No error found
			console_log("Going to index");
			header('Location: index.php');
		} else {
			$_SESSION['errorval'] = $retval;
			header('Location: signin.php');
		}

		$data = NULL;
		$row = NULL;
		$safe_pass = NULL;

		$getuser = NULL;
		$getpass = NULL;

		exit;
	}

	// If session value found
	console_log("Checking Session values");
	if (isset($_SESSION['username']) and !empty($_SESSION['username'])) {
		if (isset($title) and !empty($title) and $title == 'SignIn') {
			console_log("Already has a SignIn value no need to SignIn again...");
			header('Location: index.php');
			exit;
		}

		console_log("Session value found continuing current page...");
	} else {
		if (isset($title) and !empty($title) and $title == 'SignIn') {
			return;
		}

		console_log("No session value found going to SignIn...");
		header('Location: signin.php');
		exit;
	}
?>
