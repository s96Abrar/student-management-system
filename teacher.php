<?php
	// Setting variables for page
	$title = 'Teacher';

	require_once('header.php');
?>

<body id="page-top" data-spy="scroll" data-target=".fixed-top">
<!-- Navigation bar -->
<?php require_once('navbar.php'); ?>

<!-- Body -->
<div class="container">
	<div class="row">
		<div class="mt-4 pt-4 pl-2 pr-3 text-justify">
			<h1 class="mt-4">Teacher List</h1>
			<table class="table table-stripped table-bordered">
				<thead class="thead-light">
				<tr>
					<th>Teacher Name</th>
					<th>Designation</th>
					<th>Contact Number</th>
					<th>Email</th>
				</tr>
				</thead>

				<!-- Populate the teacher list from database using PHP -->
				<tbody>
				<?php
					require_once('dbconn.php');

					$query = "SELECT * FROM teachers_info WHERE department_info_iddepartment_info='" . $_SESSION['deptid'] . "' ORDER BY teachers_designation DESC";
					$data = mysqli_query($connection, $query);
					if ($data) {
						while ($row = $data->fetch_row()) {
							echo "" .
								"<tr>" .
								"<td>" . $row[2] . "</td>" .
								"<td>" . $row[3] . "</td>" .
								"<td>" . $row[4] . "</td>" .
								"<td>" . $row[5] . "</td>" .
								"</tr>";
						}
					} else {
						echo "<tr><td colspan='4' class='text-center'>No Data</td></tr>";
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
</div>

<!-- Footer -->
<?php require_once('footer.php'); ?>
</body>

</html>
